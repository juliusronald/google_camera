This GCam Mod base on 8.4.300 v2 Final Google Camera

1. Re-work menu list and icons.
2. Add custom lib r3.5 by Lucas.
3. New xHDR lib.
4. Forced sabre always on for all model.
5. Fix custom noise model for front cam.
6. Move merge (Super res zoom) method to libpatcher menu.
7. Add debug key signer for avoid virus warning thanks Arnova.

Best Regards
_MWPratama_